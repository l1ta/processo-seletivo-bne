/*
	2. Construa um algoritmo que receba do usuário 4 números. 
   Imprima o maior, o menor e a média deles.
*/

var readlineSync = require('readline-sync');
var numero;
var numMaior;
var numMenor;
var total = 0;

for (var i = 0; i <= 3; i++) {
	numero = parseInt(readlineSync.question('Informe um número: '));
	total = total + numero;
	if(i == 0) {
		numMenor = numero;
		numMaior = numero;
	} else {
		if(numero > numMaior) {
			numMaior = numero;
		} else if(numero < numMenor) {
			numMenor = numero;
		}
	}
}	
console.log('A média dos números informados pelo usuário é: ' + (total/4));
console.log('O MAIOR número informado é: ' + numMaior);
console.log('O MENOR número informado é:' + numMenor);
