/*
	3. Construa um algoritmo que 'instancia' um array de inteiros com os seguintes valores: 
    1,3,35,40,85,123,121,209,200,305,350.
    Após, crie uma lógica que - separe - estes números entre pares e ímpares em outros 2 arrays.

*/


var numeros = Array(1,3,35,40,85,123,121,209,200,305,350);
var numImpares = [];
var numPares = [];

for (var i = 0; i < numeros.length; i++) {
	if((numeros[i] % 2) == 0){
		numPares.push(numeros[i]);
	} else {
		numImpares.push(numeros[i]);
	}
}

for (var i = 0; i < numPares.length; i++) {
	console.log('Par ' + (numPares[i]));
}
for (var i = 0; i < numImpares.length; i++) {
	console.log('Impar ' + (numImpares[i]));
}