/*
	5. Construa um algoritmo que identifique a 
	   "quantidade" de vogais - a partir de um texto informado pelo usuário.
*/

var readlineSync = require('readline-sync');

function contarVogais(palavra) {
    var totalVogal = 0;
    var vogais = ['a', 'e', 'i', 'o', 'u'];
    var palavra = palavra.toLowerCase();
    for (var i = 0; i < palavra.length; i++ ) {
        if(vogais.indexOf(palavra[i]) != -1) {
            totalVogal++;
        }
    }
    return totalVogal;
}

var texto = readlineSync.question('Digite o texto: ');

console.log('O número de vogais é: ' + contarVogais(texto));