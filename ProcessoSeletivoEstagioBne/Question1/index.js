/*
	Construa um algoritmo que calcula o tamanho de um edifício com 
	base na "altura" de cada andar e o "número de andares" informados pelo usuário.
*/

var readline = require('readline-sync');

var tamanhoPredio;
var alturaAndar;
var numAndares;

numAndares = parseInt(readline.question('Número de andares: '));
alturaAndar = parseFloat(readline.question('Altura de cada andar em metros: '));
tamanhoPredio = numAndares*alturaAndar;

console.log(`O tamanho do edifício será de ${tamanhoPredio} metros.`);