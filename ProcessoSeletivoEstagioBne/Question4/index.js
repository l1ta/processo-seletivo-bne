/*
	4. Construa um algoritmo que recebe do usuário 5 números. 
	   ,Após recebê-los, retorne a lista de números em ordem decrescente.
*/

var readlineSync = require('readline-sync');
var numeros = [];

function ordenar(lista) {
	return lista.sort(function (primeiro, segundo) {
	  	return segundo - primeiro;
		}
	);

}

for (var i = 0; i <= 4; i++) {
	numeros[i] = parseInt(readlineSync.question('Informe o número: '));
}

var numOrdenados = ordenar(numeros);

for (var i = 0; i < numOrdenados.length; i++) {
	console.log(numOrdenados[i]);
}	